//***copied maze outlay using (Randy Cox) example during demo

let x = 0
let y = 0
let countSpaceId = 0
let begin = 0
let figureNode = document.getElementById("mazeobject")
let mazeObjClone = figureNode.cloneNode(true)

const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
]
//using Randy Cox demo prototype ***Citing JavaScript
let mazeEl = document.getElementById("maze")

const createMaze = function(blueprint) {
    for (let rowNum = 0; rowNum < blueprint.length; rowNum++) {
        const rowString = blueprint[rowNum]
        let blockDivs = ''
        for (let colNum = 0; colNum < rowString.length; colNum++) {
            const blockType = rowString[colNum]
            if (blockType === "W") {
                blockDivs += '<div class="block wall"></div>' 
            }
            else if (blockType === "S") {
                blockDivs += '<div class="block" id="str"></div>'
                y = rowNum
                x = colNum
            }
            else if (blockType === "F") {
                blockDivs += '<div class="block" id="fin"></div>'
            }
            else {
                countSpaceId = countSpaceId + 1
                blockDivs += '<div class="block"></div>'
            }
        }
        mazeEl.innerHTML += '<div class="row">' + blockDivs + '</div>'
    } 
//end using Randy Cox Prototype ***Citing JavaScript***

//Code below allows to appendChild only once in this function
        if (begin === 0) {
        mazeobject.remove()
        str.appendChild(mazeObjClone)
        begin = begin + 1
        }
     
}
createMaze(map)

document.addEventListener('keydown', logkey)

//function allows you push arrows to step through the maze

function logkey(e) {
    let arrow = ` ${e.code}`
    if (arrow === " ArrowRight" && map[y][x+1] !== "W") {
        if (map[y][x+1] === "F") {
            fin.appendChild(mazeObjClone)
            //let tspan = document.createElement("span")
            finish.innerHTML = "Congrats! You made it through the MAZE!"
            return
        } 
        if (map[y][x] !== "F") {
        let mapSplit = map[y].split('')
        mapSplit.splice(x,1,' ')
        x = x + 1
        mapSplit.splice(x,1,'S')
        let map2 = mapSplit.join('')
        map.splice(y,1,map2)
        document.getElementById("maze").innerHTML = " "
        createMaze(map)
        str.appendChild(mazeObjClone)
        }
    }
    if (arrow === " ArrowLeft" && map[y][x-1] !== "W") {
        let mapSplit = map[y].split('')
        mapSplit.splice(x,1,' ')
        x = x - 1
        if (x === -1) {
            alert('Are you trying to cheat! You cant go outside the MAZE!')
            x = x + 1
            mapSplit.splice(x,1,'S')
        }
        else {
        mapSplit.splice(x,1,'S')
        let map2 = mapSplit.join('')
        map.splice(y,1,map2)
        document.getElementById("maze").innerHTML = " "
        createMaze(map)
        str.appendChild(mazeObjClone)
        }
    }
    else if (arrow === " ArrowUp" && map[y-1][x] !== "W") {
        let mapSplit = map[y].split('')
        mapSplit.splice(x,1,' ')
        let map2 = mapSplit.join('')
        map.splice(y,1,map2)
        y = y - 1
        let mapSplit2 = map[y].split('')
        mapSplit2.splice(x,1,'S')
        let map3 = mapSplit2.join('')
        map.splice(y,1,map3)
        document.getElementById("maze").innerHTML = " "
        createMaze(map)
        str.appendChild(mazeObjClone)
    }
    else if (arrow === " ArrowDown" && map[y+1][x] !== "W") {
        let mapSplit = map[y].split('')
        mapSplit.splice(x,1,' ')
        let map2 = mapSplit.join('')
        map.splice(y,1,map2)
        y = y + 1
        let mapSplit2 = map[y].split('')
        mapSplit2.splice(x,1,'S')
        let map3 = mapSplit2.join('')
        map.splice(y,1,map3)
        document.getElementById("maze").innerHTML = " "
        createMaze(map)
        str.appendChild(mazeObjClone)
    }
}
